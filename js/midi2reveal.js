
window.addEventListener('load', function() {
  if (navigator.requestMIDIAccess) {
    window.navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure)
  }
})

const leftNotes = [36, 41, 48]
const rightNotes = [37, 39, 46]

function getMIDIMessage(message) {
  const [command, note ] = message.data
  console.log(command, note)
  switch (command) {
    case 144: // note on
    break
    case 128: // note off
      if (rightNotes.includes(note)) Reveal.right()
      if (leftNotes.includes(note)) Reveal.left()
    break
  }
}

function onMIDIFailure() {
  console.log('Could not access your MIDI devices.')
}

function onMIDISuccess(midiAccess) {
  for (var input of midiAccess.inputs.values()) {
    input.onmidimessage = getMIDIMessage
  }
}
