# 7klotz/slides

Slides to present the 7klotz project that was created during the Sevenval Hackathon Woodhack 2018.

## Install

```bash
git clone git@gitlab.com:7klotz/slides.git
cd slides
npm install
```

## Usage

```bash
npx reveal-md slides.md
```
