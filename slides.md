
welcome 👋

---

### Spaß am Build-Prozess
GitLab-Runner auf dem Raspberry Pi

---

![Woodack 2018][woodhack_logo]
[woodhack_logo]: ./img/woodhack_2018.png

---

![Borg][borg_cube]
[borg_cube]: ./img/borg.jpg

---

![klotzi][klotzi]
[klotzi]: ./img/woodhack_7klotz01.jpg

---

#### Warum?

* Unabhängig von Admins oder Infrastruktur
* Build-Prozess via `.gitlab-ci.yml` kontrollieren
* Selber skalieren mit mehr Raspberry Pis

---

#### Wie?

* Raspberry Pi
* GitLab-Runner
* Unicorn pHAT von Pimoroni
* Python für Animationen
* systemd, um LED-Matrix zu steuern
* viel Sägen, Feilen, Schleifen...

---

# DEMO

---

### Probiert viel aus!
